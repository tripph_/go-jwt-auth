package ezjwtauth

import (
	"bytes"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
)

var clientCertURL = "call SetClientCertURL with your keys URL!"

// SetClientCertURL is mandatory, and the only setup required to work with okta.
func SetClientCertURL(url string) {
	clientCertURL = url
}

type keysResponse struct {
	Keys []Key
}
type Key struct {
	Alg string
	E   string
	N   string
	Kid string
	Kty string
	Use string
}

func getKeys(keyServerURL string) []Key {
	// Checks to make sure that the clientCertURL was actually set by the user
	if clientCertURL == "call SetClientCertURL with your keys URL!" {
		log.Printf("\n\n!!! okta-go-jwt Configuration Error!!!\nCall SetClientCertURL with your keys URL first!\n")
		log.Fatalf("Example keys url: https://devdude.oktapreview.com/oauth2/default/v1/keys\n")
	}

	//resp, err := resty.R().Get(keyServerURL)
	resp, err := http.Get(keyServerURL)
	if err != nil {
		log.Fatalf("Error getting keys from server: %e\n", err)
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error reading keys from server response: %e\n", err)
	}
	resp.Body.Close()
	var keysResp keysResponse
	err = json.Unmarshal(respBody, &keysResp)
	if err != nil {
		log.Fatalf("Error parsing keys from response: %v\n", err)
	}

	return keysResp.Keys
}

func VerifyIDToken(idToken string) (map[string]interface{}, error) {
	// Thank you to: https://github.com/dkoshkin/go-firebase-verify for giving the framework for this solution.

	keys := getKeys(clientCertURL)
	parsedToken, err := jwt.Parse(idToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		kid := token.Header["kid"].(string)
		matchedKey := matchKeyToKID(keys, kid)
		rsaPublicKey := convertKey(matchedKey)
		return rsaPublicKey, nil
	})

	if err != nil {
		return nil, err
	}

	if parsedToken == nil {
		return nil, errors.New("Nil parsed token")
	}

	claims, _ := parsedToken.Claims.(jwt.MapClaims)

	return claims, nil
}

func matchKeyToKID(keys []Key, kid string) Key {
	for _, key := range keys {
		if key.Kid == kid {
			return key
		}
	}
	return keys[0]
}

func convertKey(jwk Key) interface{} {
	// Thank you to: https://github.com/dkoshkin/go-firebase-verify for giving the framework for this solution.
	// decode the base64 bytes for n
	nb, err := base64.RawURLEncoding.DecodeString(jwk.N)
	if err != nil {
		log.Fatal(err)
	}

	e := 0
	// The default exponent is usually 65537, so just compare the
	// base64 for [1,0,1] or [0,1,0,1]
	if jwk.E == "AQAB" || jwk.E == "AAEAAQ" {
		e = 65537
	} else {
		// need to decode "e" as a big-endian int
		log.Fatal("need to deocde e:", jwk.E)
	}

	pk := &rsa.PublicKey{
		N: new(big.Int).SetBytes(nb),
		E: e,
	}

	der, err := x509.MarshalPKIXPublicKey(pk)
	if err != nil {
		log.Fatal(err)
	}

	block := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: der,
	}

	var out bytes.Buffer
	pem.Encode(&out, block)

	pkey, err2 := jwt.ParseRSAPublicKeyFromPEM(out.Bytes())
	if err2 != nil {
		log.Fatalf("Error parsing cert: %e\n", err2)
	}

	rsaPublicKey := pkey

	return rsaPublicKey
}
